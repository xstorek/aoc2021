from collections import defaultdict
from typing import List, Tuple 

def parse_file() -> List[Tuple[Tuple[int, int], Tuple[int, int]]]:
    file = open("input", "r")

    lines =[]
    for line in file:
        coords = []
        for tup in line.strip().split("->"):
            xy = tup.split(",")
            coords.append((int(xy[0]), int(xy[1])))
        lines.append(tuple(coords))

    file.close()
    return lines


def count_gaysirs(include_diagonals: bool):
    counter = defaultdict(int)  # every coordinate has its count
    coords = parse_file()

    for coord in coords:
        x1 = coord[0][0]
        y1 = coord[0][1]
        x2 = coord[1][0]
        y2 = coord[1][1]

        if x1 > x2:
            xop = -1
        elif x1 == x2:
            xop = 0
        else:
            xop = 1

        if y1 > y2:
            yop = -1
        elif y1 == y2:
            yop = 0
        else:
            yop = 1

        if xop != 0 and yop != 0 and not include_diagonals:
            continue

        if xop == 0 and yop == 0:
            continue

        # counter[(x1, y1)] += 1
        counter[(x2, y2)] += 1
        x = x1  # + xop
        y = y1  # + yop
        while x != x2 or y != y2:
            counter[(x, y)] += 1
            x += xop
            y += yop

    return counter


def count(include_diagonals: bool) -> int:
    result = 0
    counter = count_gaysirs(include_diagonals)
    for key in counter:
        if counter[key] >= 2:
            result += 1
    return result


def print_bard(include_diagonals: bool):
    counter = count_gaysirs(include_diagonals)

    maxx = 0
    maxy = 0

    for x,y in counter:
        if x > maxx:
            maxx = x
        if y > maxy:
            maxy = y

    for y in range(maxy + 1):
        row = ""
        for x in range(maxx + 1):
            if (x, y) in counter:
                row += str(counter[(x, y)])
            else:
                row += "."
        print(row)
        

if __name__ == "__main__":
    print(f'Part 1: {count(False)}')
    print(f'Part 2: {count(True)}')

