from typing import List, Tuple, Set, Optional

Board = List[List[Optional[int]]]

def parse_instructions() -> List[int]:
    with open("input", "r") as file:
        instr = file.readline().strip()

    numbers = []
    for num in instr.split(","):
        numbers.append(int(num))

    return numbers


def parse_boards() -> List[Board]:
    with open("input", "r") as file:
        lines = file.readlines()

    result = []
    current_board = []
    for line in lines[2:]:
        if line == "\n":
            result.append(current_board)
            current_board = []
        else:
            new = []
            for num in line.split():
                new.append(int(num))
            current_board.append(new)

    return result


def has_won(board: Board) -> bool:
    full_line = [None] * len(board[0])
    cols = [None] * len(board[0])

    for line in board:
        if line == full_line:
            return True
        for i in range(len(line)):
            if line[i] is not None:
                cols[i] = line[i]

    if None in cols:
        return True
    return False


def score(board: Board, call) -> int:
    sum_ = 0

    for line in board:
        for num in line:
            if num is not None:
                sum_ += num

    return sum_ * call


# called numbers change to None in the boards
def bingo() -> int:
    instructions = parse_instructions()
    boards = parse_boards()
    scores = []

    for call in instructions:
        for j in range(len(boards)):
            board = boards[j]
            if has_won(board):
                continue
            for i in range(len(board)):
                for k in range(len(board[i])):
                    if board[i][k] == call:
                        board[i][k] = None
            if has_won(board):
                scores.append(score(board, call))
            boards[j] = board
    return scores


if __name__ == "__main__":
    print(f'Part 1: {bingo()[0]}')
    print(f'Part 2: {bingo()[-1]}')
