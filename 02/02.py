from typing import List, Tuple

Depth = int
Position = int

def parse_file() -> List[Tuple[str, int]]:
    file = open("input.txt", "r")

    result = []
    for line in file:
        result.append((line[:2], int(line[-2])))

    return result


def move_sub() -> Tuple[Position, Depth]:
    instructions = parse_file()

    pos = 0
    depth = 0
    for command, value in instructions:
        if command == "fo":
            pos += value
        elif command == "do":
            depth += value
        else:
            depth -= value

    return pos, depth


def multiply(x: Tuple[Position, Depth]) -> int:
    return x[0] * x[1]


def move_sub_better() -> Tuple[Position, Depth]:
    instructions = parse_file()

    pos = 0
    depth = 0
    aim = 0
    for command, value in instructions:
        if command == "fo":
            pos += value
            depth += value * aim
        elif command == "do":
            aim += value
        else:
            aim -= value

    return pos, depth


if __name__ == "__main__":
    print(f'Part 1: {multiply(move_sub())}')
    print(f'Part 2: {multiply(move_sub_better())}')
